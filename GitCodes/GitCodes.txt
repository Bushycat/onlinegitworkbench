megansweetie@bushmaster:~$ cd GitWorkbench/

megansweetie@bushmaster:~/GitWorkbench$ ls -l
total 28
drwxr-xr-x 2 megansweetie megansweetie 4096 Nov 27 19:43 GitCodes
drwxr-xr-x 2 megansweetie megansweetie 4096 Nov 27 19:26 HTML
drwxr-xr-x 2 megansweetie megansweetie 4096 Nov 27 19:53 Java
drwxr-xr-x 2 megansweetie megansweetie 4096 Nov 27 19:42 LinuxCodes
drwxr-xr-x 2 megansweetie megansweetie 4096 Nov 27 19:43 MySQL
drwxr-xr-x 2 megansweetie megansweetie 4096 Nov 27 19:43 Python
drwxr-xr-x 2 megansweetie megansweetie 4096 Nov 30 19:01 Test

megansweetie@bushmaster:~/GitWorkbench$ 

megansweetie@bushmaster:~/GitWorkbench$ git status
On branch master
Your branch is up to date with 'origin/master'.

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	.gitignore
	GitCodes/
	MySQL/

nothing added to commit but untracked files present (use "git add" to track)

megansweetie@bushmaster:~/GitWorkbench$ git add GitCodes/*.*

megansweetie@bushmaster:~/GitWorkbench$ git add MySQL/*.*

megansweetie@bushmaster:~/GitWorkbench$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	new file:   GitCodes/GitCodes.txt
	new file:   MySQL/SQLCodes.txt

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	.gitignore

megansweetie@bushmaster:~/GitWorkbench$ git commit -m "Version1"
[master 3a87ba9] Version1
 2 files changed, 81 insertions(+)
 create mode 100644 GitCodes/GitCodes.txt
 create mode 100644 MySQL/SQLCodes.txt

megansweetie@bushmaster:~/GitWorkbench$ git status
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	.gitignore

nothing added to commit but untracked files present (use "git add" to track)

megansweetie@bushmaster:~/GitWorkbench$ git pull
Already up to date.

megansweetie@bushmaster:~/GitWorkbench$ git push
Password for 'https://Bushycat@bitbucket.org': 

Enumerating objects: 7, done.
Counting objects: 100% (7/7), done.
Delta compression using up to 4 threads
Compressing objects: 100% (4/4), done.
Writing objects: 100% (6/6), 1.43 KiB | 1.43 MiB/s, done.
Total 6 (delta 0), reused 0 (delta 0)
To https://bitbucket.org/Bushycat/onlinegitworkbench.git
   5c35a5e..3a87ba9  master -> master

megansweetie@bushmaster:~/GitWorkbench$ git log

commit 3a87ba9d02a5c24c8f4a672108a4031842fe1751 (HEAD -> master, origin/master)
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Mon Nov 30 19:22:46 2020 +0000

    Version1

commit 5c35a5eff5eef2141f2c0b681e78765bd9808648
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Fri Nov 27 19:50:49 2020 +0000

    Testing Java Code

commit 5adb2681f5da85ba33b5686f4226c81aee468546
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Fri Nov 27 19:35:03 2020 +0000

    SARC

commit 9302a3f5aa65b904ba08c646fce795992c79dea1
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Fri Nov 27 19:31:07 2020 +0000

    Magic

commit 269ab225c0554e036e656bfde6fb4b4e8ab55be0
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Fri Nov 27 19:20:42 2020 +0000

    Changes Made

commit 431d755c55888d2546b37b417805d5228164b4ea
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Fri Nov 27 19:06:34 2020 +0000

    Content is now inside test directory

commit 233b55f6de178c4d994789462727e4da581b44aa
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Thu Nov 26 19:40:07 2020 +0000

    Test1

commit c003f5a5f99c620aacb392391f72503802d15958
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Thu Nov 26 19:27:26 2020 +0000

    Full Stack

commit 3ec40d3dbc517e07a7a095e94d80b298965516df
Merge: e2ce220 2d7f6ff
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Thu Nov 26 19:21:05 2020 +0000

    Goon

commit e2ce2203dfa8a3713c9a90b900b4798ddbd692bf
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Thu Nov 26 19:05:10 2020 +0000

    Megan Sweeties Health

commit 2d7f6ff5c550e18c9bfa71baaadc6d5b56c13452
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Wed Nov 25 19:41:01 2020 +0000

    Lines added to Megan Sweetie

commit 0d111cba954f3dd3251a1ed481dc3bcfd4e74776
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Wed Nov 18 19:26:17 2020 +0000

    Megan

commit a847e9667e47ff8f5559fe24e42f89fc6eef9f18
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Wed Nov 18 18:56:06 2020 +0000

    2nd Version of the Draft

commit 0278e9b58c64b19d1b5b39b5b259a0c3b9a58aab
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Tue Nov 17 19:17:38 2020 +0000

    Showcasing commit functionality of Git

megansweetie@bushmaster:~/GitWorkbench$ 

megansweetie@bushmaster:~/GitWorkbench$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   GitCodes/GitCodes.txt

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	.gitignore

no changes added to commit (use "git add" and/or "git commit -a")
megansweetie@bushmaster:~/GitWorkbench$ git add GitCodes/*.*
megansweetie@bushmaster:~/GitWorkbench$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	modified:   GitCodes/GitCodes.txt

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	.gitignore

megansweetie@bushmaster:~/GitWorkbench$ git commit -m "Version2"
[master 6e5c6b0] Version2
 1 file changed, 155 insertions(+)
megansweetie@bushmaster:~/GitWorkbench$ git status
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	.gitignore

nothing added to commit but untracked files present (use "git add" to track)
megansweetie@bushmaster:~/GitWorkbench$ git pull
Already up to date.
megansweetie@bushmaster:~/GitWorkbench$ git push
Password for 'https://Bushycat@bitbucket.org': 
Enumerating objects: 7, done.
Counting objects: 100% (7/7), done.
Delta compression using up to 4 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (4/4), 1.84 KiB | 1.84 MiB/s, done.
Total 4 (delta 0), reused 0 (delta 0)
To https://bitbucket.org/Bushycat/onlinegitworkbench.git
   3a87ba9..6e5c6b0  master -> master
megansweetie@bushmaster:~/GitWorkbench$ git status
On branch master
Your branch is up to date with 'origin/master'.

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	.gitignore

nothing added to commit but untracked files present (use "git add" to track)
megansweetie@bushmaster:~/GitWorkbench$ git log
commit 6e5c6b07dbf620bcbfc39dfd36e6c46361d5cfbe (HEAD -> master, origin/master)
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Mon Nov 30 19:27:45 2020 +0000

    Version2

commit 3a87ba9d02a5c24c8f4a672108a4031842fe1751
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Mon Nov 30 19:22:46 2020 +0000

    Version1

commit 5c35a5eff5eef2141f2c0b681e78765bd9808648
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Fri Nov 27 19:50:49 2020 +0000

    Testing Java Code

commit 5adb2681f5da85ba33b5686f4226c81aee468546
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Fri Nov 27 19:35:03 2020 +0000

    SARC

commit 9302a3f5aa65b904ba08c646fce795992c79dea1
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Fri Nov 27 19:31:07 2020 +0000

    Magic

commit 269ab225c0554e036e656bfde6fb4b4e8ab55be0
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Fri Nov 27 19:20:42 2020 +0000

    Changes Made

commit 431d755c55888d2546b37b417805d5228164b4ea
Author: M Ashraf <13ushmast3r@gmail.com>


megansweetie@bushmaster:~$ cd GitWorkbench/

megansweetie@bushmaster:~/GitWorkbench$ ls -l
total 28
drwxr-xr-x 2 megansweetie megansweetie 4096 Nov 30 19:35 GitCodes
drwxr-xr-x 2 megansweetie megansweetie 4096 Nov 27 19:26 HTML
drwxr-xr-x 2 megansweetie megansweetie 4096 Nov 27 19:53 Java
drwxr-xr-x 2 megansweetie megansweetie 4096 Dec  1 19:31 LinuxCodes
drwxr-xr-x 2 megansweetie megansweetie 4096 Nov 30 19:19 MySQL
drwxr-xr-x 2 megansweetie megansweetie 4096 Nov 30 19:38 Python
drwxr-xr-x 2 megansweetie megansweetie 4096 Nov 30 19:01 Test

megansweetie@bushmaster:~/GitWorkbench$ git status
On branch master
Your branch is up to date with 'origin/master'.

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	.gitignore
	LinuxCodes/

nothing added to commit but untracked files present (use "git add" to track)

megansweetie@bushmaster:~/GitWorkbench$ git add LinuxCodes/*.*

megansweetie@bushmaster:~/GitWorkbench$ git status

On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	new file:   LinuxCodes/LinuxBasicCodes.txt

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	.gitignore

megansweetie@bushmaster:~/GitWorkbench$ git commit -m 'LinuxCommands1'
[master 5f2b04f] LinuxCommands1
 1 file changed, 375 insertions(+)
 create mode 100644 LinuxCodes/LinuxBasicCodes.txt

megansweetie@bushmaster:~/GitWorkbench$ git status
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	.gitignore

nothing added to commit but untracked files present (use "git add" to track)

megansweetie@bushmaster:~/GitWorkbench$ git pull
Already up to date.

megansweetie@bushmaster:~/GitWorkbench$ git push
Password for 'https://Bushycat@bitbucket.org': 

Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 4 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (4/4), 3.73 KiB | 3.73 MiB/s, done.
Total 4 (delta 1), reused 0 (delta 0)
To https://bitbucket.org/Bushycat/onlinegitworkbench.git
   ff9f157..5f2b04f  master -> master

megansweetie@bushmaster:~/GitWorkbench$

megansweetie@bushmaster:~/GitWorkbench$ git log

commit 5f2b04f0fc050e0b04bcbe2b1408fe00f7384722 (HEAD -> master, origin/master)
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Tue Dec 1 19:32:59 2020 +0000

    LinuxCommands1

commit ff9f157a250283d3889c23ae60d1b46033c007c8
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Mon Nov 30 19:48:55 2020 +0000

    MathVersion1

commit ede563b63b66fec96018ca71b0e4bd6d1988a43c
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Mon Nov 30 19:36:12 2020 +0000

    Version2

commit 6e5c6b07dbf620bcbfc39dfd36e6c46361d5cfbe
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Mon Nov 30 19:27:45 2020 +0000

    Version2

commit 3a87ba9d02a5c24c8f4a672108a4031842fe1751
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Mon Nov 30 19:22:46 2020 +0000

    Version1

commit 5c35a5eff5eef2141f2c0b681e78765bd9808648
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Fri Nov 27 19:50:49 2020 +0000

    Testing Java Code

commit 5adb2681f5da85ba33b5686f4226c81aee468546
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Fri Nov 27 19:35:03 2020 +0000

    SARC

commit 9302a3f5aa65b904ba08c646fce795992c79dea1
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Fri Nov 27 19:31:07 2020 +0000

    Magic

commit 269ab225c0554e036e656bfde6fb4b4e8ab55be0
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Fri Nov 27 19:20:42 2020 +0000

    Changes Made

commit 431d755c55888d2546b37b417805d5228164b4ea
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Fri Nov 27 19:06:34 2020 +0000

    Content is now inside test directory

commit 233b55f6de178c4d994789462727e4da581b44aa
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Thu Nov 26 19:40:07 2020 +0000

    Test1

commit c003f5a5f99c620aacb392391f72503802d15958
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Thu Nov 26 19:27:26 2020 +0000

    Full Stack

commit 3ec40d3dbc517e07a7a095e94d80b298965516df
Merge: e2ce220 2d7f6ff
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Thu Nov 26 19:21:05 2020 +0000

    Goon

commit e2ce2203dfa8a3713c9a90b900b4798ddbd692bf
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Thu Nov 26 19:05:10 2020 +0000

    Megan Sweeties Health

commit 2d7f6ff5c550e18c9bfa71baaadc6d5b56c13452
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Wed Nov 25 19:41:01 2020 +0000

    Lines added to Megan Sweetie

commit 0d111cba954f3dd3251a1ed481dc3bcfd4e74776
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Wed Nov 18 19:26:17 2020 +0000

    Megan

commit a847e9667e47ff8f5559fe24e42f89fc6eef9f18
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Wed Nov 18 18:56:06 2020 +0000

    2nd Version of the Draft

commit 0278e9b58c64b19d1b5b39b5b259a0c3b9a58aab
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Tue Nov 17 19:17:38 2020 +0000

    Showcasing commit functionality of Git

megansweetie@bushmaster:~/GitWorkbench$ 

megansweetie@bushmaster:~$ cd GitWorkbench/

megansweetie@bushmaster:~/GitWorkbench$ ls -l
total 28
drwxr-xr-x 2 megansweetie megansweetie 4096 Dec 17 19:50 GitCodes
drwxr-xr-x 2 megansweetie megansweetie 4096 Nov 27 19:26 HTML
drwxr-xr-x 2 megansweetie megansweetie 4096 Nov 27 19:53 Java
drwxr-xr-x 2 megansweetie megansweetie 4096 Dec  1 19:31 LinuxCodes
drwxr-xr-x 2 megansweetie megansweetie 4096 Dec  8 19:29 MySQL
drwxr-xr-x 2 megansweetie megansweetie 4096 Nov 30 19:38 Python
drwxr-xr-x 2 megansweetie megansweetie 4096 Nov 30 19:01 Test

megansweetie@bushmaster:~/GitWorkbench$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   HTML/Test.html

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	.gitignore

no changes added to commit (use "git add" and/or "git commit -a")

megansweetie@bushmaster:~/GitWorkbench$ git add HTML/*.*

megansweetie@bushmaster:~/GitWorkbench$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	modified:   HTML/Test.html

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	.gitignore

megansweetie@bushmaster:~/GitWorkbench$ git commit -m "MyResumeVersion1"
[master 303e4cd] MyResumeVersion1
 1 file changed, 40 insertions(+)

megansweetie@bushmaster:~/GitWorkbench$ git status
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	.gitignore

nothing added to commit but untracked files present (use "git add" to track)

megansweetie@bushmaster:~/GitWorkbench$ git pull
Already up to date.

megansweetie@bushmaster:~/GitWorkbench$ git push
Password for 'https://Bushycat@bitbucket.org': 
Enumerating objects: 7, done.
Counting objects: 100% (7/7), done.
Delta compression using up to 4 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (4/4), 677 bytes | 677.00 KiB/s, done.
Total 4 (delta 1), reused 0 (delta 0)
To https://bitbucket.org/Bushycat/onlinegitworkbench.git
   864c5d9..303e4cd  master -> master

megansweetie@bushmaster:~/GitWorkbench$ git log
commit 303e4cd5998a157d0e587516839252e18c0232f3 (HEAD -> master, origin/master)
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Thu Dec 17 19:51:25 2020 +0000

    MyResumeVersion1

commit 864c5d9e17466765f133921dd3d0814f82cdb330
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Tue Dec 8 19:32:40 2020 +0000

    SQLCodes Version1

commit 1b03cd37b82ad334e45f05ae848cf4ff3f91931a
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Tue Dec 1 19:38:51 2020 +0000

    GitCodes

commit 5f2b04f0fc050e0b04bcbe2b1408fe00f7384722
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Tue Dec 1 19:32:59 2020 +0000

    LinuxCommands1

commit ff9f157a250283d3889c23ae60d1b46033c007c8
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Mon Nov 30 19:48:55 2020 +0000

    MathVersion1

commit ede563b63b66fec96018ca71b0e4bd6d1988a43c
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Mon Nov 30 19:36:12 2020 +0000

    Version2

commit 6e5c6b07dbf620bcbfc39dfd36e6c46361d5cfbe
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Mon Nov 30 19:27:45 2020 +0000

    Version2

commit 3a87ba9d02a5c24c8f4a672108a4031842fe1751
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Mon Nov 30 19:22:46 2020 +0000

    Version1

commit 5c35a5eff5eef2141f2c0b681e78765bd9808648
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Fri Nov 27 19:50:49 2020 +0000

    Testing Java Code

commit 5adb2681f5da85ba33b5686f4226c81aee468546
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Fri Nov 27 19:35:03 2020 +0000

    SARC

commit 9302a3f5aa65b904ba08c646fce795992c79dea1
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Fri Nov 27 19:31:07 2020 +0000

    Magic

commit 269ab225c0554e036e656bfde6fb4b4e8ab55be0
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Fri Nov 27 19:20:42 2020 +0000

    Changes Made

commit 431d755c55888d2546b37b417805d5228164b4ea
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Fri Nov 27 19:06:34 2020 +0000

    Content is now inside test directory

commit 233b55f6de178c4d994789462727e4da581b44aa
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Thu Nov 26 19:40:07 2020 +0000

    Test1

commit c003f5a5f99c620aacb392391f72503802d15958
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Thu Nov 26 19:27:26 2020 +0000

    Full Stack

commit 3ec40d3dbc517e07a7a095e94d80b298965516df
Merge: e2ce220 2d7f6ff
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Thu Nov 26 19:21:05 2020 +0000

    Goon

commit e2ce2203dfa8a3713c9a90b900b4798ddbd692bf
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Thu Nov 26 19:05:10 2020 +0000

    Megan Sweeties Health

commit 2d7f6ff5c550e18c9bfa71baaadc6d5b56c13452
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Wed Nov 25 19:41:01 2020 +0000

    Lines added to Megan Sweetie

commit 0d111cba954f3dd3251a1ed481dc3bcfd4e74776
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Wed Nov 18 19:26:17 2020 +0000

    Megan

commit a847e9667e47ff8f5559fe24e42f89fc6eef9f18
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Wed Nov 18 18:56:06 2020 +0000

    2nd Version of the Draft

commit 0278e9b58c64b19d1b5b39b5b259a0c3b9a58aab
Author: M Ashraf <13ushmast3r@gmail.com>
Date:   Tue Nov 17 19:17:38 2020 +0000

    Showcasing commit functionality of Git

megansweetie@bushmaster:~/GitWorkbench$ 








