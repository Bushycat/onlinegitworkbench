/* Testing boolean logics */

public class booleanMath{
	public static void main (String[] args){
		double x, y, z;
		boolean p, q, r;

		x = 0.5;
		y = 1.5;
		z = 0.001;

		double k = ((x + y) * Math.pow((x + y + z), 0.5)) / 2.0;

		if ((x >= y && y <= z) || (x <= y && y >= z))
			System.out.println("TRUE");
		else
			System.out.println("FALSE");

		System.out.println(k);
	}
}